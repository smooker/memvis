#include "asynccin.h"
#include <iostream>

using namespace std;

int asynccin::thread(void*)
{
    std::cin >> val;
    hasval = 2;
    return 2;           //silent the compiler
}

bool asynccin::begininput()
{
    if( hasval != 0 ) return false;
    hasval = 1;
    SDL_CreateThread( thread, nullptr, static_cast<void*>(nullptr));
    return true;
}

bool asynccin::hasinput()
{
    return hasval == 2;
}

char asynccin::reapinput()
{
    hasval = false;
    return val;
}

