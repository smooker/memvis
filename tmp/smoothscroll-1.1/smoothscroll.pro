# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

TARGET = smoothscroll
TEMPLATE = app

CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt



HEADERS =

SOURCES = \
   $$PWD/sscroll.c


INCLUDEPATH += \
    /usr/include/SDL

LIBS += -lGL -lSDL

DEFINES = \
    STDC_HEADERS=1 \
    HAVE_SYS_TYPES_H=1 \
    HAVE_SYS_STAT_H=1 \
    HAVE_STDLIB_H=1 \
    HAVE_STRING_H=1 \
    HAVE_MEMORY_H=1 \
    HAVE_STRINGS_H=1 \
    HAVE_INTTYPES_H=1 \
    HAVE_STDINT_H=1 \
    HAVE_UNISTD_H=1 \
    HAVE_SDL_SDL_OPENGL_H=1
    HAVE_OPENGL=1 \
    GNU_SOURCE=1 \
    _REENTRANT

# -g -O2

#gcc -DPACKAGE_NAME=\"\" -DPACKAGE_TARNAME=\"\" -DPACKAGE_VERSION=\"\" -DPACKAGE_STRING=\"\" -DPACKAGE_BUGREPORT=\"\" -DPACKAGE_URL=\"\" -DPACKAGE=\"smoothscroll\" -DVERSION=\"1.1\" -DSTDC_HEADERS=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_MEMORY_H=1 -DHAVE_STRINGS_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_UNISTD_H=1 -DHAVE_SDL_SDL_OPENGL_H=1 -I. -I.     -g -O2  -DHAVE_OPENGL -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -c `test -f sscroll.c || echo './'`sscroll.c
#gcc  -g -O2  -DHAVE_OPENGL -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT   -o sscroll  sscroll.o -lGL -lm -L/usr/lib/x86_64-linux-gnu -lSDL
#-rwxrwxr-x 1 smooker smooker  61712 май 14 06:08 sscroll
#-rw-r--r-- 1 smooker smooker  17755 май 13 22:54 sscroll.c
#-rw-rw-r-- 1 smooker smooker 104888 май 14 06:08 sscroll.o
