#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_ttf.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>

#include "asynccin.h"
#include "glfuncs.h"

using namespace std;

static SDL_Event Event;

static bool Running = true;
static bool redraw = true;

static int scrollLeft=0;
static int scrollRight=0;

static long position=0x000;
static long fileLength=0;

static uint8_t bytesPerColumn=1;                  //bytes per column

static uint8_t* buffer;

static SDL_Window *MainWindow;                    // Declare a pointer
static SDL_Renderer* renderer = nullptr;

//oshte svinshtina
static TTF_Font* MyFont;

static SDL_GLContext mainContext;

//
static Uint32 timeStepMs;
static Uint32 timeLastMs;
static Uint32 timeCurrentMs;
static Uint32 timeDeltaMs;
static Uint32 timeAccumulatedMs;

//
void dot(SDL_Renderer* renderer, int x, int y, uint8_t byteval)
{
    SDL_Rect size;
    SDL_RenderGetViewport(renderer, &size);

//    printf("rdr:%d, %d\r\n", size.y, size.h);

    SDL_Rect r;

    for(int i=7;i>=0;i--) {
        r.x = (8*x)+1;
        r.y = size.h-(8*(y+i+1))+1;
        r.w = 5;
        r.h = 5;

        if ( (byteval & (1 << i) ) ) {
            fillrect_gl(&r, 0xffffffff);
        }	else {
            fillrect_gl(&r, 0xff555555);
        }
    }
}

//
void drawBuffer(SDL_Renderer* renderer, uint8_t* buffer, long start, int line = 0)
{
    SDL_Rect size;
    SDL_RenderGetViewport(renderer, &size);

    GLuint TextureID = 0;

//    SDL_Color TextOverlayColor = {255, 25, 255, 25};  // this is the color in rgb format, maxing out all would give you the color white, and it will be your text's color

//    char msg[80];
//    sprintf(msg, "0x%08lx", start);
//    SDL_Surface* surfaceMessage = TTF_RenderText_Blended(MyFont, msg, TextOverlayColor); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

//    SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture
//    SDL_SetTextureBlendMode(Message, SDL_BLENDMODE_BLEND);
//    SDL_SetTextureAlphaMod(Message, 128);

//    glGenTextures(1, &TextureID);
//    glBindTexture(GL_TEXTURE_2D, TextureID);

//    if(surfaceMessage->format->BytesPerPixel == 4) {
//        Mode = GL_RGBA;
//    }

//    SDL_Rect Message_rect; //create a rect
//    Message_rect.x = 0;  //controls the rect's x coordinate
//    Message_rect.y = size.h-64; // controls the rect's y coordinte
//    Message_rect.w = 512; // controls the width of the rect   //fixme da se opredelia ot fonta
//    Message_rect.h = 64; // controls the height of the rect

    //printf("drawBuffer: start:%d\n", start);
    int y=0;
    int x=0;

    long pos=start;

    //thin red line
    SDL_Rect r;
    r.x = 0;
    r.y = size.h-(64*(line+1))-1;
    r.w = size.w;
    r.h = 1;
    fillrect_gl(&r, 0xffff0000);

    while (1) {
        // check if we are out of boundaries on x
        if ( (x*8) >= (size.w-8) )
            break;

        dot(renderer, x, y+(line*8), buffer[pos]);       //8 vertikalni bits
//        printf("p1:%d\r\n", y);
        pos++;
        x++;
    }
    //text overlay
//    glBegin(GL_QUADS);
//        glTexCoord2f(0, 0); glVertex3f(x, y, 0);
//        glTexCoord2f(1, 0); glVertex3f(x + 8, y, 0);
//        glTexCoord2f(1, 1); glVertex3f(x + 8, y + 8, 0);
//        glTexCoord2f(0, 1); glVertex3f(x, y + 64, 0);
//    glEnd();
//    SDL_RenderCopy(renderer, Message, nullptr, &Message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture

//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surfaceMessage->w, surfaceMessage->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surfaceMessage->pixels);

//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Don't forget too free your surface and texture
//    SDL_FreeSurface(surfaceMessage);
//    SDL_DestroyTexture(Message);
}

//
void OnLoop()
{
    if (scrollLeft) {
        position-=bytesPerColumn;
        redraw = true;
    }

    if (position < 0) {
        scrollLeft=0;
        position = 0;
    }

    if (scrollRight) {
        position+=bytesPerColumn;
        redraw = true;
    }

    if (position > (fileLength-1)) {
        scrollRight=0;
        position = fileLength-1;
    }

    if (redraw == true) {
        redraw = false;
        glClear(GL_COLOR_BUFFER_BIT);     //zashto da go chistim ?
        drawBuffer(renderer, buffer, position, 0);
        drawBuffer(renderer, buffer, position+0x100, 1);
        drawBuffer(renderer, buffer, position+0x200, 2);
        drawBuffer(renderer, buffer, position+0x300, 3);
        drawBuffer(renderer, buffer, position+0x400, 4);
        drawBuffer(renderer, buffer, position+0x500, 5);
        drawBuffer(renderer, buffer, position+0x600, 6);
        drawBuffer(renderer, buffer, position+0x700, 7);
        SDL_GL_SwapWindow(MainWindow);
    }
}

//
void OnEvent(SDL_Event* Event)
{
    if (Event->type == SDL_QUIT) {
        Running = false;
    }

    if (Event->type == SDL_KEYDOWN) {
        switch(Event->key.keysym.sym)
        {
        case SDLK_a:
            scrollLeft=1;
            scrollRight=0;
            printf("left\r\n");
            break;
        case SDLK_d:
            scrollRight=1;
            scrollLeft=0;
            printf("right\r\n");
            break;
        case SDLK_s:
            scrollRight=0;
            scrollLeft=0;
            printf("stop\r\n");
            break;
        case SDLK_q:
            printf("q:%d\r\n", bytesPerColumn);
            redraw=1;
            bytesPerColumn--;      //fixme
            break;
        case SDLK_e:
            printf("e:%d\r\n", bytesPerColumn);
            redraw=1;
            bytesPerColumn++;      //fixme
            break;
        case SDLK_7:
            position -= 0x1000;
            redraw = true;
            break;
        case SDLK_8:
            position += 0x1000;
            redraw = true;
            printf("0x%08lx\n", position);
            break;

        case SDLK_ESCAPE:
            Running=false;
            break;
        default:
            break;
        }
    }

    if (Event->type == SDL_KEYUP) {
        switch(Event->key.keysym.sym)
        {
        case SDLK_ESCAPE:
            Running=false;
            break;
        default:
            break;
        }
    }
}

//stdin
void processInput()
{
    const Uint8* keystate = SDL_GetKeyboardState(nullptr);
    //continuous-response keys
    if ( keystate[SDL_SCANCODE_LEFT] ) {
        position-=bytesPerColumn;

        if (position < 0)
            position = 0;
        redraw = 1;
    }
    //
    if ( keystate[SDL_SCANCODE_RIGHT] ) {
        position+=bytesPerColumn;

        if (position > (fileLength-1))
            position=fileLength-1;
        redraw = 1;
    }
    //
    while(SDL_PollEvent(&Event) != 0) {
        OnEvent(&Event);
    }
}

int main(int argc, char** argv) {

    system ("/bin/stty raw");

    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS);              // Initialize SDL2

    atexit(SDL_Quit);

    //here. init
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 0);

    glewExperimental = GL_TRUE;

    TTF_Init();

    MyFont = TTF_OpenFont("TerminusTTF-4.39.ttf", 128);

    if (MyFont == nullptr) {
        printf("Unable to load font: %s \r\n", TTF_GetError());
        // Handle the error here.
    }

    timeStepMs = static_cast<Uint32>(1000.f / 25); //eg. 30Hz

    SDL_DisplayMode dm;

    if (SDL_GetDesktopDisplayMode(0, &dm) != 0)
    {
         SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
         return 1;
    }

    //fixme. dual monitor

    // Create an application window with the following settings:
    MainWindow = SDL_CreateWindow(
                "An SDL2 window",                  // window title
                0,
                24,
                dm.w,                               // width, in pixels
                dm.h-24,                               // height, in pixels
                SDL_WINDOW_BORDERLESS | SDL_WINDOW_OPENGL | SDL_WINDOW_ALWAYS_ON_TOP
                );

    // Check that the window was successfully created
    if (MainWindow == nullptr) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\r\n", SDL_GetError());
        return 1;
    }

    SDL_SetWindowInputFocus(MainWindow);

    mainContext = SDL_GL_CreateContext(MainWindow);

    // Setup renderer
    renderer =  SDL_CreateRenderer( MainWindow, -1, SDL_RENDERER_ACCELERATED);

    GLenum err=glewInit();
    if(err!=GLEW_OK) {
      // Problem: glewInit failed, something is seriously wrong.
      cout << "glewInit failed: " << glewGetErrorString(err) << endl;
      exit(1);
    }

    glClearColor(0.6, 0.1, 0.1, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    SDL_GL_SetSwapInterval(-1);

//    SDL_GL_SwapWindow(MainWindow);

    SDL_Delay(100);

//    SDL_GetRendererOutputSize(renderer, &screenWidth, &screenHeight);

//    printf ("W:%d\tH:%d\r\n", screenWidth, screenHeight);

//    SDL_Rect displayRect;
//    displayRect.w=dm.w;
//    displayRect.h=dm.h-24;
//    displayRect.x=0;
//    displayRect.y=24;
//    SDL_RenderSetViewport( renderer, &displayRect );

    // Set render color to red ( background will be rendered in this color )
    SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );

    // Clear winow
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    ifstream is("./icm.bin", std::ifstream::binary );        // Input file stream object

    if (is.good()) {
        printf("Stream good\r\n");

        // get length of file:
        is.seekg (0, is.end);
        long length = is.tellg();
        fileLength = length;
        is.seekg (0, is.beg);

        buffer = new uint8_t [length];

        std::cout << "Reading " << length << " characters... " << "\r\n" ;
        // read data as a block:
        is.read ((char*)buffer, length);

        if (is)
            std::cout << "all characters read successfully." << "\r\n";
        else
            std::cout << "error: only " << is.gcount() << " could be read" << "\r\n";
        is.close();

//        drawBuffer(renderer, buffer, bytesPerColumn, position);
    } else {
        cout << "Error!";
        exit(0);
    }

    is.close();

    printf("waiting...\r\n");

    while(Running) {
        timeLastMs = timeCurrentMs;
        timeCurrentMs = SDL_GetTicks();
        timeDeltaMs = timeCurrentMs - timeLastMs;
        timeAccumulatedMs += timeDeltaMs;

        while (timeAccumulatedMs >= timeStepMs)
        {
            asynccin::begininput();
            //
            if( asynccin::hasinput() ) {
                //
                if ( asynccin::reapinput() == '0' ) {
                    Running = false;
                }
                //
                if ( asynccin::reapinput() == '7' ) {
                    position -= 0x1000;
                    redraw = true;
                }
                //
                if ( asynccin::reapinput() == '8' ) {
                    position += 0x1000;
                    redraw = true;
                }
                //screenshots
                if ( asynccin::reapinput() == '-' ) {
                    char msg[80];
                    sprintf(msg, "screenshot_at_0x%08lx.bmp", position);

                    SDL_DisplayMode dm;

                    if (SDL_GetDesktopDisplayMode(0, &dm) != 0)
                    {
                         SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
                         return 1;
                    }

                    SDL_Surface *sshot = SDL_CreateRGBSurface(0, dm.w, dm.h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
                    SDL_RenderReadPixels(renderer, nullptr, SDL_PIXELFORMAT_ARGB8888, sshot->pixels, sshot->pitch);
                    SDL_SaveBMP(sshot, msg);
                    SDL_FreeSurface(sshot);
                }
            }
//            printf("HERE: %d\t%d\r\n", scrollLeft, scrollRight);      //debug only
            timeAccumulatedMs -= timeStepMs;
            processInput();
            OnLoop();
        }
    }

    delete[] buffer;

    SDL_GL_DeleteContext(mainContext);

    TTF_CloseFont(MyFont);

    // Close and destroy the window
    SDL_DestroyWindow(MainWindow);
    SDL_DestroyRenderer(renderer);

    // Clean up
    SDL_Quit();
    system ("/bin/stty cooked");

    return 0;
}
